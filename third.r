
#' The 'third' function
#'
#' This function is used to create .
#'
#' Arguments of the 'rkg' function are 'ode' class, noisy observation, and kernel type. It return the interpolation for each of the ode states. The Ode parameters are estimated using gradient matching, and the results are stored in the 'ode' class as the ode_par attribute.
#' @param kkk ode class object.
#' @param bbb rihs class object 
#' @param crtype optimisation scheme type. User can choose 'i' or '3'. 'i' is for fast iterative scheme and '3' for optimising the ode parameters and interpolation coefficients simultaneously.
#' @param woption If the warping scheme is done before using the ode regularisation, user can choose 'w' otherwise just leave this option empty.
#' @param dtilda The gradient of Warping function. This variable is only added if user want to combine warping and the ode regularisation.
#' @return return list containing :
#' \itemize{ 
#'	\item{} oppar - the ode parameters estimation. 
#'	\item{} rk3 - rkhs class which include the updated interpolation results.}   
#' @export
#' @examples
#' load('examples.Rdata')
#' ## create a ode class object
#' init_par = rep(c(0.1),4)
#' init_yode = t(y_no)#kkk0$y_ode
#' init_t = t_no#kkk0$t
#' kkk = ode$new(1,fun=LV_fun,grfun=LV_grlNODE,t=init_t,ode_par= init_par, y_ode=init_yode )
#'
#' ## Use function 'rkg' to estimate the Ode parameters.
#' ktype ='rbf'
#' rkgres = rkg(kkk,y_no,ktype)
#' ## show the results of Ode parameter estimation using standard gradient matching
#' kkk$ode_par
#'
#' @author Mu Niu \email{mu.niu@plymouth.ac.uk}


third = function(lam,kkk,bbb,crtype,woption,dtilda)
{
  nst = dim(kkk$y_ode)[1]
  npar = length(kkk$ode_par)

  if(missing(woption)) {
        woption = 'nw'
    } 

   ode_m = kkk$clone()
   iterp = rkg3$new()
   iterp$odem=ode_m
	for( st in 1:nst)
	{
	  rk1 = bbb[[st]]$clone()
	  iterp$add(rk1)
	}
     if(woption=='nw'){
	if(crtype=='i'){	
	   	   iterp$iterate(20,3,lam) 
	   	   oppar = iterp$odem$ode_par
	   }
	else if(crtype=='i3'){ 
	   	   iterp$iterate(20,3,lam)
	       oppar3=iterp$opfull( lam)  
	       oppar = tail(oppar3[[1]],npar)  
	    }
	 else if( crtype=='3'){
	       oppar3=iterp$opfull( lam)
	       oppar = tail(oppar3[[1]],npar)
	   }
    } else if(woption=='w')
    { print('wwww')
     	if(crtype=='i'){	
	   	   iterp$witerate(20,3,dtilda,lam)
	   	   oppar = iterp$odem$ode_par
	   }
	 else if(crtype=='i3'){ 
	   	   iterp$witerate(20,3,dtilda,lam)
	       oppar3=iterp$wopfull( lam,dtilda ) 
	       oppar = tail(oppar3[[1]],npar)   
	    }
	 else if( crtype=='3'){
	       oppar3=iterp$wopfull( lam,dtilda )
	       oppar = tail(oppar3[[1]],npar)
	   }
    }
  return(list("oppar" = oppar,"rk3"=iterp))
}

