
crossv = function(lam,kkk,bbb,crtype,y_no,woption,resmtest,dtilda,fold)
{
nst = dim(kkk$y_ode)[1]
npar = length(kkk$ode_par)
lop=length(lam)
## randomly shuffle the data
set.seed(19573)
radsp = sample( length( kkk$t ) ) 
Xdata = kkk$t[ radsp ]    
Ydata=y_no[radsp,]
## create 2 equally size folds
if(missing(fold)) 
 {
fold = 2
}

folds <- cut(seq(1,length(Xdata)),breaks=fold,labels=FALSE)

if(missing(woption)) 
 {
    woption = 'nw'
 } else {    woption
 Xdata = resmtest[,radsp]
 wgrad = dtilda[,radsp]
 }

ress = array(c(0))

for( ll in 1:lop) 
 {  
   crerror = 0
   for(fdi in 1:fold)
   {
	  testIndexes = which(folds==fdi,arr.ind=TRUE)
	  y_test = t(Ydata[testIndexes,])
      y_train = Ydata[-testIndexes,]
      ntrain = length(kkk$t) - length(testIndexes)

      if(woption=='nw'){
	  testData = Xdata[testIndexes]
	  trainData = Xdata[-testIndexes]
	  } else {
	  testData = Xdata[,testIndexes]
	  trainData = Xdata[,-testIndexes]
	  trainGrad = wgrad[,-testIndexes]
	  testGrad = wgrad[,testIndexes]
	  }
      n_o = ntrain 
      
      kkk1 = ode$new(1,fun=kkk$ode_fun,grfun=kkk$gr_lNODE,t=trainData,y_ode=t(y_train) )

      if(woption == 'nw') {
      ktype='rbf'
      #kkk1 = ode$new(1,fun=BP_fun,grfun=BP_grlNODE,t=trainData,y_ode=t(y_train) )
      rkgres = rkg(kkk1,y_train,ktype)
      bbb1 = rkgres$bbb
      
      ode_m = kkk$clone()
	  iterp = rkg3$new()
	  iterp$odem=ode_m
		for( st in 1:nst)
		 {
		  rk1 = bbb1[[st]]$clone()
		  iterp$add(rk1)
		 } 
      } else
      {
     ### learn interpolates in warped time domain	
      bbbw = c()
	  intp = c()
	  grad = c()
        for(wi in 1: nst)
        {
	      ann1w = RBF$new(1)
	      bbb1w = rkhs$new(t(y_train)[wi,], trainData[wi,],rep(1,ntrain),1,ann1w)
	      bbb1w$skcross(5)

	      bbbw=c(bbbw,bbb1w)
	      intp = rbind(intp,bbbw[[wi]]$predict()$pred)
	      grad = rbind(grad,bbbw[[wi]]$predict()$grad*trainGrad[wi,])
	    }
       inipar= rep(0.1,npar)
       kkk1$optim_par( inipar, intp, grad )
       
       ode_m = kkk1$clone()
	   iterp = rkg3$new()
	   iterp$odem=ode_m
		for( st in 1:nst)
		 {
		  rk1 = bbbw[[st]]$clone()
		  iterp$add(rk1)
		 } 
      }
   
	  if(woption=='nw'){
		if(crtype=='i'){	
		   	   iterp$iterate(20,3,lam[ll]) 
		   }
		else if(crtype=='i3'){ 
		   	   iterp$iterate(20,3,lam[ll])
		       oppar=iterp$opfull( lam[ll])     
		    }
		 else if( crtype=='3'){
		       oppar=iterp$opfull( lam[ll])
		   }
	    } else if(woption=='w')
	    { print('wwww')
	     	if(crtype=='i'){	
		   	   iterp$witerate(20,3,trainGrad,lam[ll])
		   }
		 else if(crtype=='i3'){ 
		   	   iterp$witerate(20,3,trainGrad,lam[ll])
		       oppar=iterp$wopfull( lam[ll],trainGrad )    
		    }
		 else if( crtype=='3'){
		       oppar=iterp$wopfull( lam[ll],trainGrad )
		   }
	    }
	
		for(st in 1:nst)
		 {
		 if(woption=='nw'){
		    crerror = crerror+sum( (iterp$rk[[st]]$predictT(testData)$pred - y_test[st,])^2 )
		   } else if(woption=='w')
		   {
		   	crerror = crerror+sum( (iterp$rk[[st]]$predictT(testData[st,])$pred - y_test[st,])^2 )
		   }
		 }
     }

   ress[ll] = crerror
 }

 lam = lam[which(ress==min(ress))]
return(list("lam"=lam,"crloss"=ress))
}

