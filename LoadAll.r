
##############################################################
library(R6)
source('kernel.r')
source('rkhs.r')
source('rk3g.r')
source('ode.r')
source('WarpSin.r')
source('warpfun.r')
source('crossv.r')
source('warpInitLen.r')
source('third.r')
source('rkg.r')

library(deSolve)
library(pracma)
library(mvtnorm)
library(pspline)
