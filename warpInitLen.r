
warpInitLen= function(peod,eps,rkgres,lens)
{
 if(missing(lens)) {
        lens = c(4,5)
    } 
###### warp 1st state
lambda_t= 50    ## the weight of fixing the end of interval 
n_o = length(rkgres$intp[1,])
wres=c()
 for(i in 1:length(rkgres$bbb))
 {
  y_c = rkgres$intp[i,]
  wsigm = Sigmoid$new(1)
  bbb = Warp$new( y_c,rkgres$bbb[[i]]$t,rep(1,n_o),lambda_t,wsigm)
  wresi<- bbb$slowWarp(lens,peod[i],eps)
  wres<-c(wres,wresi$len)
 }
return(wres)  
}
