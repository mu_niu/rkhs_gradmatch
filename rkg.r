
#' The 'rkg' function
#'
#' This function is used to create 'rkhs' class object and estimate Ode parameters using standard gradient matching.
#'
#' Arguments of the 'rkg' function are 'ode' class, noisy observation, and kernel type. It return the interpolation for each of the ode states. The Ode parameters are estimated using gradient matching, and the results are stored in the 'ode' class as the ode_par attribute.
#' @param kkk ode class object.
#' @param y_no matrix of noisy observations. The row represent the ode states and the column represents the time points.
#' @param ktype kernel type. User can choose 'rbf' or 'mlp' kernel.
#' @return return list containing :
#' \itemize{ 
#'	\item{} intp - interpolation for each ode state  
#'	\item{} bbb - rkhs class for each ode state }   
#' @export
#' @examples
#' load('examples.Rdata')
#' ## create a ode class object
#' init_par = rep(c(0.1),4)
#' init_yode = t(y_no)#kkk0$y_ode
#' init_t = t_no#kkk0$t
#' kkk = ode$new(1,fun=LV_fun,grfun=LV_grlNODE,t=init_t,ode_par= init_par, y_ode=init_yode )
#'
#' ## Use function 'rkg' to estimate the Ode parameters.
#' ktype ='rbf'
#' rkgres = rkg(kkk,y_no,ktype)
#' ## show the results of Ode parameter estimation using standard gradient matching
#' kkk$ode_par
#'
#' @author Mu Niu \email{mu.niu@plymouth.ac.uk}

rkg = function(kkk,y_no,ktype)
{
nst = dim(kkk$y_ode)[1]
npar = length(kkk$ode_par)
bbb=c()
intp= c()
grad= c()
n_o = max( dim( y_no) )
for (st in 1:nst)
 {
	if (ktype=='rbf'){
		ann1 = RBF$new(1)
		bbb1 = rkhs$new(t(y_no)[st,],kkk$t,rep(1,n_o),1,ann1)
		bbb1$skcross(c(2) ) 
	} else if(ktype=='mlp'){
		ann1 = MLP$new(c(5,5))
		bbb1 = rkhs$new(t(y_no)[st,],kkk$t,rep(1,n_o),1,ann1)
		bbb1$mkcross(c(5,5))
	}
	bbb=c(bbb,bbb1)
	intp = rbind(intp,bbb[[st]]$predict()$pred)
	grad = rbind(grad,bbb[[st]]$predict()$grad)
 }

inipar= rep(0.1,npar)
kkk$optim_par( inipar, intp, grad )
return(list("bbb"=bbb,"intp"=intp))
}