#' The 'Kernel' class object
#'
#' This a abstract class     provide the rbf kernel function and the 1st order derivative of rbf kernel function. 
#' @docType class
#' @importFrom R6 R6Class
#' @export
#' @keywords data
#' @return an  \code{\link{R6Class}} object which can be used for the rkhs interpolation.
#' @format \code{\link{R6Class}} object.
#' @field k_par the hyper-parameter of kernel (the lengthscale parameter of rbf kernel).
#' @section Methods:
#' \describe{
#'   \item{\code{kern()}}{This method is used to calculate the kernel function given two one dimensional real inputs.}   
#'   \item{\code{dkdt()}}{This method is used to calculate the 1st order derivative of kernel function given two one dimensional real inputs.} }
#' @export
#' @examples
#' load('examples.Rdata')
#' ## create a 'ode' class object with noisy observation.
#' init_par = rep(c(0.1),4)
#' init_yode = t(y_no)
#' init_t = t_no
#' kkk = ode$new(1,fun=LV_fun,grfun=LV_grlNODE,t=init_t,ode_par= init_par, y_ode=init_yode )
#'
#'
#' @author Mu Niu, \email{mu.niu@plymouth.ac.uk}


library(R6)


Kernel<-R6Class("Kernel",
  public = list(
    k_par=NULL,

    initialize = function(k_par = NULL) {
      self$k_par <- k_par
      self$greet()
    },
    
    greet = function() {
    },

    kern = function (t1,t2) {
    },

    dkd_kpar = function(t1,t2) {
    },

  dkdt = function (t1,t2) {  
    }

  )
)


RBF <- R6Class("RBF",
  inherit = Kernel,
  public = list(

    greet = function() {
      cat(paste0("RBF len is", self$k_par, ".\n"))
    },

    set_k_par = function(val) {
      self$k_par <- val
    },


    kern = function (t1,t2) {
      x=t1-t2
      1/2*exp( -x^2/(2*self$k_par) )
    },

    dkd_kpar = function(t1,t2) {
      x=t1-t2
      1/2*exp( -x^2/(2*self$k_par) ) * x^2 / (2*self$k_par^2)*self$k_par
    },

   dkdt = function (t1,t2) {
     x=(t1-t2)
     #1/2*exp( -x^2/(2*self$k_par) ) * 1/self$k_par*(-x)
     1/2*exp( -x^2/(2*self$k_par) ) * 1/self$k_par*(-x)
    }

  )

)


MLP <- R6Class("MLP",
  inherit = Kernel,
  public = list(

    greet = function() {
      cat(paste0("MLP is", self$k_par, ".\n"))
    },

    set_k_par = function(val) {
      self$k_par <- val
    },

    kern = function (t1,t2) {
      asin(   (self$k_par[1]*t1*t2+self$k_par[2]) /   sqrt(self$k_par[1]*t1^2+self$k_par[2]+1) / sqrt(self$k_par[1]*t2^2+self$k_par[2]+1)    )   
    },

    dkd_kpar = function(t1,t2) {
      w=self$k_par[1]; bb = self$k_par[2]
      x=t1;xt=t2
      num=(w*x*xt+bb)
      denom =  sqrt(w*x^2+bb+1)*sqrt(w*xt^2+bb+1)
      db = ( 1/denom - 1/2*num/denom^3*((x^2+xt^2)*w+2*bb+2) ) * 1/sqrt(1-( num/denom)^2 )  *bb    
      dw= ( x*xt/denom - 1/2*num/denom^3 * ( (w*x^2+bb+1)*xt^2 + (w*xt^2+bb+1)*x^2 ) ) * 1/sqrt(1-( num/denom)^2 )  *w
      c(dw,db)
    },

    dkdt = function (t1,t2) {
      w=self$k_par[1]; bb = self$k_par[2]
      x=t1;xt=t2
      num=(w*x*xt+bb)
      denom =  sqrt(w*x^2+bb+1)*sqrt(w*xt^2+bb+1)
      vec= xt^2*w+bb+1
      ( xt/denom - ( x*num*vec ) / denom^3 ) *w* 1/sqrt(1-( num/denom)^2 )  
    }

  )

)



Sigmoid <- R6Class("sigmoid",
  inherit = Kernel,
  public = list(

    #greet = function() {
    #  cat(paste0("sigmoid len is", self$k_par, ".\n"))
    #},

    set_k_par = function(val) {
      self$k_par <- val
    },

    kern = function (t1,t2) {
      x=t1-t2
      1/( 1+exp(-x*self$k_par) )
    },

    dkd_kpar = function(t1,t2) {
      x=t1-t2
      l = self$k_par
      if( (x*l)> -20 ){
      ds=-( 1+exp(-x*l) )^(-2)* exp(-x*l) *(-l)
       }
       else {
      ds = exp(x*l+log(l))
       }
     ds
    },

   dkdt = function (t1,t2) {
     x=(t1-t2)
     l = self$k_par
     bas = exp(-x*l)
      if(is.infinite(bas)){
       dsdt = 0#( 1+exp(-x*l) )^(-2)* exp(-x*l) *l#0
        }else
       {
       dsdt = (1+bas)^(-2)*l*bas
       }
      return(dsdt)
     }

  )

)

