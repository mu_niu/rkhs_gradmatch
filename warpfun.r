

warpfun =function(kkkrkg,bbb,peod,eps,fixlens,y_no,testData)
{
   kkk= kkkrkg$clone()
   if(missing(testData)) 
   {
   	flagtest = 2
    } else {flagtest = 1}

	bbbw = c()
	dtilda = c()
	intp = c()
	grad = c()
	resmtest =c()
	wfun=c()
	nst = length(bbb)
    
	for( st in 1:nst)
	{
		###### warp 1st state
		p0=peod[st]            ## a guess of the period of warped signal
		lambda_t= 50    ## the weight of fixing the end of interval 
		y_c = bbb[[st]]$predict()$pred ##y_use[1,]

		#### fix len
		fixlen = fixlens[st]#2.9  !!!!!!!!!!!!!!!!!!!!! give a constant for testing

		wsigm = Sigmoid$new(1)
		n_o = max( dim( kkk$y_ode) )
		bbbs = Warp$new( y_c,kkk$t,rep(1,n_o),lambda_t,wsigm)
		ppp = bbbs$warpSin( fixlen, 10,p0,eps )   ## 3.9 70db

		### learnign warping function using mlp
		t_me= bbbs$tw #- mean(bbbs$tw)
		ben = MLP$new(c(5,5)) 
		rkben = rkhs$new(t(t_me),kkk$t,rep(1,n_o),1,ben)
		rkben$mkcross(c(5,5))
		resm = rkben$predict()
		if(flagtest==1) 
		{
		 resmtest = rbind(resmtest, rkben$predictT(testData)$pred)
	    } 
		### learn interpolates in warped time domain
		ann1w = RBF$new(1)
		bbb1w = rkhs$new(t(y_no)[st,],resm$pred,rep(1,n_o),1,ann1w)
		bbb1w$skcross(5)

		dtilda =rbind(dtilda,resm$grad)
		bbbw=c(bbbw,bbb1w)
	    wfun=c(wfun,rkben)
	    
		intp = rbind(intp,bbbw[[st]]$predict()$pred)
		grad = rbind(grad,bbbw[[st]]$predict()$grad*resm$grad)
	}

    inipar= rep(0.1,length(kkk$ode_par))
    kkk$optim_par( inipar, intp, grad )
  return(list("dtilda"=dtilda,"bbbw"=bbbw,"wtime"=resmtest,"wfun"=wfun,"wkkk"=kkk))
}
